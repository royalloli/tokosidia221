//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model.Tokosidia
{
    using System;
    using System.Collections.Generic;
    
    public partial class Customer
    {
        public int ID { get; set; }
        public string NAMA { get; set; }
        public string NO_TLP { get; set; }
        public string EMAIL { get; set; }
        public Nullable<int> GENDER { get; set; }
        public Nullable<int> INDENTITAS_TYPE { get; set; }
        public string NO_INDENTITAS { get; set; }
        public string ALAMAT { get; set; }
        public string KODE_PROVINSI { get; set; }
        public string KODE_KOTA { get; set; }
        public string KODE_KECAMATAN { get; set; }
        public string KODE_KELURAHAN { get; set; }
        public Nullable<bool> ISDELETE { get; set; }
        public string CREATED_BY { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public Nullable<System.DateTime> UPDATED_DATE { get; set; }
        public Nullable<int> ROLE_ID { get; set; }
    }
}
