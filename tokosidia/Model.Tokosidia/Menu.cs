//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Model.Tokosidia
{
    using System;
    using System.Collections.Generic;
    
    public partial class Menu
    {
        public int Id { get; set; }
        public string Menu_Name { get; set; }
        public string Menu_Url { get; set; }
        public string Menu_Icon { get; set; }
        public Nullable<int> Menu_Order { get; set; }
        public Nullable<int> Menu_Parent { get; set; }
        public string Menu_Type { get; set; }
        public Nullable<bool> is_delete { get; set; }
        public string Created_by { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Updated_by { get; set; }
        public string Updated_Date { get; set; }
    }
}
