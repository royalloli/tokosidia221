﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using Repo.Tokosidia;

namespace tokosidia.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            VMCustomer result = LoginRepo.prosesLogin(email, password);

            if (result != null)
            {
                Session["name"] = result.NAMA;
                Session["role_id"] = result.ROLE_ID;
                Session["id_customer"] = result.ID;
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
        }

        public static List<VMMenu> getMenuByRoleId()
        {
            int roleID = int.Parse(System.Web.HttpContext.Current.Session["role_id"].ToString());
            List<VMMenu> menu = LoginRepo.getMenu(roleID);

            return menu;
        }
    }
}