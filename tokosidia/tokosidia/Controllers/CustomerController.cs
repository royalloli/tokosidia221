﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using Repo.Tokosidia;

namespace tokosidia.Controllers
{
    [Tokosidia.SessionCheck.SessionCheck]
    public class CustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            if (Session["name"] == null)
            {
                return Redirect("~/Login/Login/");
            }
            else
            {
                List<VMCustomer> listCustomer = new List<VMCustomer>();
                listCustomer = CustomerRepo.getAllData();
                return View(listCustomer);
            }
        }

        //view Add data customer
        public ActionResult Create()
        {
            ViewBag.ListProvinsi = CustomerRepo.getAllDataPovinsi();
            ViewBag.ListKota = new List<Kota>();
            ViewBag.listKecamatan = new List<Kecamatan>();
            ViewBag.listKelurahan = new List<Kelurahan>();
            //List<Provinsi> listProvinsi = new List<Provinsi>();
            //listProvinsi = CustomerRepo.getAllDataPovinsi();
            //ViewData["provinsi"] = listProvinsi;

            return View();
        }

        [HttpPost]
        //Post add customer
        public ActionResult Create(Customer customers)
        {
            string result = CustomerRepo.createData(customers);

            if (result == "ok")
            {
                return Json(new { message = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else if (result == "sameemail")
            {
                return Json(new { message = "Gagal, email sudah ada di database" }, JsonRequestBehavior.AllowGet);
            }
            else if (result == "sametelp")
            {
                return Json(new { message = "Gagal, nomor telpon sudah ada di database" }, JsonRequestBehavior.AllowGet);
            }
            else if (result == "sameidentitas")
            {
                return Json(new { message = "Gagal, nomor identitas sudah ada di database" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getListKota(string Kode_Provinsi)
        {
            return Json(CustomerRepo.getListKota(Kode_Provinsi), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getListKecamatan(string Kode_Kota)
        {
            return Json(CustomerRepo.getListKecamatan(Kode_Kota), JsonRequestBehavior.AllowGet);
        }
        public JsonResult getListKelurahan(string Kode_Kecamatan)
        {
            return Json(CustomerRepo.getListKelurahan(Kode_Kecamatan), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id)
        {
            Customer customers = CustomerRepo.getDataById(id);
            //customers = CustomerRepo.getDataById(id);

            //ViewBag.listGender = CustomerRepo.getDataGeneral("gender");
            //ViewBag.listIdentityType = CustomerRepo.getDataGeneral("gender");
            ViewBag.listProvinsi = CustomerRepo.getAllDataPovinsi();

            if (customers.KODE_PROVINSI != null)
            {
                ViewBag.listKota = CustomerRepo.getListKota(customers.KODE_PROVINSI);
            }
            if (customers.KODE_KOTA != null)
            {
                ViewBag.listKecamatan = CustomerRepo.getListKecamatan(customers.KODE_KOTA);
            }
            if (customers.KODE_KECAMATAN != null)
            {
                ViewBag.listKelurahan = CustomerRepo.getListKelurahan(customers.KODE_KECAMATAN);
            }

            return View(customers);
        }

        [HttpPost]
        public ActionResult Edit(Customer customers)
        {
            string result = CustomerRepo.updateData(customers);

            if (result == "ok")
            {
                return Json(new { message = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {

            string result = CustomerRepo.deleteData(id);

            if (result == "ok")
            {
                return Json(new { hasil = "berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { hasil = "gagal" }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}