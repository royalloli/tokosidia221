﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using Repo.Tokosidia;
using PagedList;

namespace tokosidia.Controllers
{
    [Tokosidia.SessionCheck.SessionCheck]
    public class CatalogController : Controller
    {
        // GET: Catalog
        public ActionResult Index()
        {
            if (Session["name"] == null)
            {
                return Redirect("~/Login/Login/");
            }
            else
            {
                List<M_tbl_Barang> listBarang = new List<M_tbl_Barang>();
                listBarang = BarangRepo.getAllData();

                return View(listBarang);
            }
        }

        public ActionResult Details(int id)
        {
            M_tbl_Barang Barang = new M_tbl_Barang();

            Barang = BarangRepo.getDataById(id);

            return View(Barang);
        }

        [HttpPost]
        public ActionResult Buy(VMTransaksi trnsaksi)
        {
            trnsaksi.Customer_Id = Convert.ToInt16(Session["id_customer"]);
            string result = CatalogRepo.transaksiBarang(trnsaksi);

            if (result == "ok")
            {
                return Json(new { message = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}