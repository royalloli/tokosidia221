﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using Repo.Tokosidia;
using PagedList;
using System.IO;

namespace tokosidia.Controllers
{
    [Tokosidia.SessionCheck.SessionCheck]
    public class BarangController : Controller
    {
        // GET: Barang
        public ActionResult Index(string searchString, string currentFilter, string sortOrder, int? page)
        {
            if (Session["name"] == null)
            {
                return Redirect("~/Login/Login/");
            }
            else
            {
                List<M_tbl_Barang> listBarang = new List<M_tbl_Barang>();
                listBarang = BarangRepo.getAllData();

                if (searchString != null)
                {
                    page = 1;
                }
                else
                {
                    searchString = currentFilter;
                }

                ViewBag.currentFilter = searchString;

                ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

                if (!string.IsNullOrEmpty(searchString))
                {
                    listBarang = listBarang.Where(a => a.Nama_Barang.ToLower().Contains(searchString.ToLower())).ToList();
                }
                else if (sortOrder == "name_desc")
                {
                    listBarang = listBarang.OrderByDescending(a => a.Nama_Barang).ToList();
                }
                else if (sortOrder == null)
                {
                    listBarang = listBarang.OrderBy(a => a.Nama_Barang).ToList();
                }

                int pageSize = 10;
                int pageNumber = (page ?? 1);

                return View(listBarang.ToPagedList(pageNumber,pageSize));
            }
        }

        public ActionResult Edit(int id)
        {
            M_tbl_Barang Barang = new M_tbl_Barang();

            Barang = BarangRepo.getDataById(id);

            return View(Barang);
        }

        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(M_tbl_Barang barang)
        {
            string result = BarangRepo.createData(barang);

            if (result == "ok")
            {
                return Json(new { message = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }

            //return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Edit(M_tbl_Barang barang)
        {

            //M_tbl_Barang barangLama = new M_tbl_Barang();

            string result = BarangRepo.updateData(barang);

            if (result == "ok")
            {
                return Json(new { message = "Berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { message = "Gagal" }, JsonRequestBehavior.AllowGet);
            }
            
            //return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Delete(int id)
        {

            string result = BarangRepo.deleteData(id);

            if (result == "ok")
            {
                return Json(new { hasil = "berhasil" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { hasil = "gagal" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Upload()
        {
            var file = Request.Files[0];
            var fileName = Path.GetFileName(file.FileName);

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images") + "\\" + fileName;

            file.SaveAs(path);

            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            byte[] data = new byte[(int)fileStream.Length];
            fileStream.Read(data, 0, data.Length);

            VMUpload upload = new VMUpload();

            upload.File_Name = fileName;
            upload.File_Url = path;

            return Json(new { upload, path = Convert.ToBase64String(data) }, JsonRequestBehavior.AllowGet);
        }
    }
}