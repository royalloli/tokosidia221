﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using Repo.Tokosidia;

namespace tokosidia.Controllers
{
    [Tokosidia.SessionCheck.SessionCheck]
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            List<VMTransaksi> listCart = new List<VMTransaksi>();
            int id = Convert.ToInt16(Session["id_customer"]);
            ViewBag.totalBelanja = CartRepo.getTotalBelanja(id);
            listCart = CartRepo.getDataCartCustomer(id);

            return View(listCart);
        }
    }
}