﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Tokosidia
{
    public class VMTransaksi
    {
        public string Kode_Transaksi { get; set; }
        public int? id_Barang { get; set; }
        public int Quantity { get; set; }
        public decimal Harga { get; set; }
        public decimal hargaBarang { get; set; }
        

        public int Customer_Id { get; set; }
        public Nullable<bool> isCheckout { get; set; }
    }
}
