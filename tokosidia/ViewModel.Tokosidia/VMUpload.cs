﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Tokosidia
{
    public class VMUpload
    {
        public string File_Name { get; set; }

        public string File_Url { get; set; }
    }
}
