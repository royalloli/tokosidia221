﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Tokosidia
{
    public class VMMenu
    {
        public int Menu_Id { get; set; }
        public string Menu_Name { get; set; }
        public string Menu_Url { get; set; }
        public string Menu_Icon { get; set; }
        public Nullable<int> Menu_Order { get; set; }
        public Nullable<int> Menu_Parent { get; set; }
        public string Menu_Type { get; set; }
        public int? ROLE_ID { get; set; }
        public string Role_Name { get; set; }
    }
}
