﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Tokosidia
{
    public class VMCustomer
    {
        public int ID { get; set; }
        public string NAMA { get; set; }
        public string NO_TLP { get; set; }
        public string EMAIL { get; set; }
        public Nullable<int> GENDER { get; set; }
        public Nullable<int> INDENTITAS_TYPE { get; set; }
        public string NO_INDENTITAS { get; set; }
        public string ALAMAT { get; set; }
        public string KODE_PROVINSI { get; set; }
        public string KODE_KOTA { get; set; }
        public string KODE_KECAMATAN { get; set; }
        public string KODE_KELURAHAN { get; set; }
        public Nullable<int> ROLE_ID { get; set; }

        public string NAMA_PROVINSI { get; set; }

        public string NAMA_KOTA { get; set; }

        public string NAMA_KECAMATAN { get; set; }

        public string NAMA_KELURAHAN { get; set; }
    }
}
