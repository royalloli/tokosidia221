﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using System.Data.Entity;


namespace Repo.Tokosidia
{
    public class CustomerRepo
    {
        public static List<VMCustomer> getAllData()
        {
            List<VMCustomer> data = new List<VMCustomer>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = (from cs in db.Customers
                        where cs.ISDELETE == false
                        join pr in db.Provinsis on cs.KODE_PROVINSI equals pr.Kode_Provinsi
                        join kt in db.Kotas on cs.KODE_KOTA equals kt.Kode_Kota
                        join kc in db.Kecamatans on cs.KODE_KECAMATAN equals kc.Kode_Kecamatan
                        join kl in db.Kelurahans on cs.KODE_KELURAHAN equals kl.Kode_Kelurahan

                        select new VMCustomer
                        {
                            ID = cs.ID,
                            NAMA = cs.NAMA,
                            EMAIL = cs.EMAIL,
                            ALAMAT = cs.ALAMAT,
                            NO_INDENTITAS = cs.NO_INDENTITAS,
                            NO_TLP = cs.NO_TLP,
                            GENDER = cs.GENDER,
                            KODE_PROVINSI = pr.Kode_Provinsi,
                            NAMA_PROVINSI = pr.Nama_Provinsi,
                            KODE_KOTA = kt.Kode_Kota,
                            NAMA_KOTA = kt.Nama_Kota,
                            KODE_KECAMATAN = kc.Kode_Kecamatan,
                            NAMA_KECAMATAN = kc.Nama_Kecamatan,
                            KODE_KELURAHAN = kl.Kode_Kelurahan,
                            NAMA_KELURAHAN = kl.Nama_Kelurahan
                        }).ToList();
                //listCustomer = db.Customers.Where(a => a.ISDELETE == false).ToList(); //Lamda
            }

            return data;
        }
        public static List<Provinsi> getAllDataPovinsi()
        {
            List<Provinsi> listProvinsi = new List<Provinsi>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                listProvinsi = db.Provinsis.ToList();
            }

            return listProvinsi;
        }
        public static List<Kota> getListKota(string Kode_Provinsi)
        {
            List<Kota> data = new List<Kota>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db.Kotas.Where(a => a.Kode_Provinsi == Kode_Provinsi).ToList();
            }

            return data;
        }

        public static List<Kecamatan> getListKecamatan(string Kode_Kota)
        {
            List<Kecamatan> data = new List<Kecamatan>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db.Kecamatans.Where(a => a.Kode_Kota == Kode_Kota).ToList();
            }
            return data;
        }

        public static List<Kelurahan> getListKelurahan(string Kode_Kecamatan)
        {
            List<Kelurahan> data = new List<Kelurahan>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db.Kelurahans.Where(a => a.Kode_Kecamatan == Kode_Kecamatan).ToList();
            }

            return data;
        }

        public static string createData(Customer customers)
        {

            try
            {
                List<VMCustomer> data = new List<VMCustomer>();


                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {

                    data = (from cs in db.Customers
                                 select new VMCustomer
                                 {
                                     EMAIL = cs.EMAIL,
                                     NO_INDENTITAS = cs.NO_INDENTITAS,
                                     NO_TLP = cs.NO_TLP

                                 }).ToList();
                    
                    List<string> identsama = data.Select(a => a.NO_INDENTITAS).ToList();
                    List<string> emailsama = data.Select(a => a.EMAIL).ToList();
                    List<string> tlpsama = data.Select(a => a.NO_TLP).ToList();

                    customers.ISDELETE = false;
                    customers.CREATED_BY = "System";
                    customers.CREATED_DATE = DateTime.Now;

                    string returnData = "ok";
                    

                    for (int i = 0; i < data.Count(); i++)
                    {
                        if(customers.NO_INDENTITAS == identsama[i].ToString()) {
                            returnData = "sameidentitas";
                            break;
                        }
                        if (customers.NO_TLP == tlpsama[i].ToString())
                        {
                            returnData = "sametelp";
                            break;
                        }
                        if (customers.EMAIL == emailsama[i].ToString())
                        {
                            returnData = "sameemail";
                            break;
                        }
                    }

                    db.Customers.Add(customers);
                    db.SaveChanges();

                    return returnData;
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }

        }
        public static Customer getDataById(int id)
        {
            Customer data = new Customer();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db
                    .Customers.Where(a => a.ID == id).FirstOrDefault();
            }

            return data;
        }

        public static string updateData(Customer customers)
        {
            try
            {
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    Customer customerlama = new Customer();

                    customerlama = getDataById(customers.ID);
                    customerlama.NO_INDENTITAS = customers.NO_INDENTITAS;
                    customerlama.NAMA = customers.NAMA;
                    customerlama.NO_TLP = customers.NO_TLP;
                    customerlama.EMAIL = customers.EMAIL;
                    customerlama.ALAMAT = customers.ALAMAT;
                    customerlama.GENDER = customers.GENDER;
                    customerlama.KODE_PROVINSI = customers.KODE_PROVINSI;
                    customerlama.KODE_KOTA = customers.KODE_KOTA;
                    customerlama.KODE_KECAMATAN = customers.KODE_KECAMATAN;
                    customerlama.KODE_KELURAHAN = customers.KODE_KELURAHAN;


                    customerlama.UPDATED_BY = "System";
                    customerlama.UPDATED_DATE = DateTime.Now;

                    db.Entry(customerlama).State = EntityState.Modified;
                    db.SaveChanges();

                    return "ok";
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public static string deleteData(int id)
        {
            try
            {
                Customer customers = new Customer();
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    customers = getDataById(id);
                    customers.ISDELETE = true;
                    customers.UPDATED_BY = "System";
                    customers.UPDATED_DATE = DateTime.Now;

                    db.Entry(customers).State = EntityState.Modified;
                    db.SaveChanges();

                    return "ok";
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        
    }

}
