﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using System.Data.Entity;

namespace Repo.Tokosidia
{
    public class LoginRepo
    {
        public static VMCustomer prosesLogin(string email, string password)
        {
            VMCustomer data = new VMCustomer();
            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = (from cs in db.Customers
                        where cs.EMAIL == email
                        select new VMCustomer
                        {
                            NAMA = cs.NAMA,
                            EMAIL = cs.EMAIL,
                            ROLE_ID = cs.ROLE_ID,
                            ID = cs.ID
                        }).FirstOrDefault();
            }

            return data;
        }
        public static List<VMMenu> getMenu(int ROLE_ID)
        {
            List<VMMenu> menu = new List<VMMenu>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                menu = (from ma in db.Menu_Access
                        join mn in db.Menus on ma.Menu_id equals mn.Id into mnTemp
                        join cs in db.Customers on ma.Role_Id equals cs.ROLE_ID into csTemp
                        from mnt in mnTemp.DefaultIfEmpty()
                        from cst in csTemp.DefaultIfEmpty()
                        where cst.ROLE_ID == ROLE_ID
                        select new VMMenu
                        {
                            Menu_Id = mnt.Id,
                            Menu_Name = mnt.Menu_Name,
                            Menu_Url = mnt.Menu_Url,
                            Menu_Order = mnt.Menu_Order,
                            Menu_Type = mnt.Menu_Type,
                            ROLE_ID = cst.ROLE_ID
                        }).ToList();
            }
            return menu;
        }
    }
}
