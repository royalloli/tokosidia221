﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using System.Data.Entity;
using System.Data.SqlClient;

namespace Repo.Tokosidia
{
    public class CatalogRepo
    {
        public static List<M_tbl_Barang> getAllData()
        {
            List<M_tbl_Barang> listBarang = new List<M_tbl_Barang>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                listBarang = db.M_tbl_Barang.Where(a => a.is_delete == false).ToList();
            }

            return listBarang;
        }

        public static M_tbl_Barang getDataById(int id)
        {
            M_tbl_Barang data = new M_tbl_Barang();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db.M_tbl_Barang.Where(a => a.id_Barang == id).FirstOrDefault();
            }

            return data;
        }

        public static string transaksiBarang(VMTransaksi transaksi)
        {
            try
            {
                Order_Header dataHeader = new Order_Header();
                Order_Detail dataDetail = new Order_Detail();
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    string data = "TX0001";
                    string coba = getLastDataKodeTransaksi();

                    if (lastID() != null)
                    {
                        string dataString = lastID().Substring(lastID().Length - 4, 4);
                        //string dataString = getLastDataKodeTransaksi().Substring(getLastDataKodeTransaksi().Length - 4, 4);
                        int dataInt = Convert.ToInt32(dataString) + 1;
                        string joinString = "0000" + dataInt.ToString();
                        data = "TX" + joinString.Substring(joinString.Length - 4, 4);
                    }

                    dataHeader.Kode_Transaksi = data;
                    dataHeader.Customer_Id = transaksi.Customer_Id;
                    dataHeader.isCheckout = false;
                    db.Order_Header.Add(dataHeader);
                    db.SaveChanges();

                    dataDetail.id_Barang = transaksi.id_Barang;
                    dataDetail.Kode_Transaksi = dataHeader.Kode_Transaksi;
                    dataDetail.Quantity = transaksi.Quantity;
                    dataDetail.Harga = transaksi.hargaBarang * transaksi.Quantity;
                    db.Order_Detail.Add(dataDetail);
                    db.SaveChanges();
                }
                return "ok";

            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }
        }
        public static string lastID()
        {
            string connectionString = GetConnectionString();
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand command = new SqlCommand("select top(1) Kode_Transaksi from Order_Header order by Kode_Transaksi desc", connection);

            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            string data = reader["Kode_Transaksi"].ToString();

            reader.Close();

            return data;
        }

        static private string GetConnectionString()
        {
            return "Data Source=localhost;Initial catalog=DBTokosidia;" + "Integrated Security=True;";
        }

        public static string getLastDataKodeTransaksi()
        {
            VMTransaksi data = new VMTransaksi();
            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = (from oh in db.Order_Header
                        orderby oh.Kode_Transaksi descending
                        select new VMTransaksi
                        {
                            Kode_Transaksi = oh.Kode_Transaksi
                        }).FirstOrDefault();
            }
            string hasil = data.Kode_Transaksi;
            return hasil;
        }
    }
}
