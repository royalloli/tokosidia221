﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using System.Data.Entity;

namespace Repo.Tokosidia
{
    public class BarangRepo
    {
        public static List<M_tbl_Barang> getAllData()
        {
            List<M_tbl_Barang> listBarang = new List<M_tbl_Barang>();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                listBarang = db.M_tbl_Barang.Where(a => a.is_delete == false).ToList();
            }

            return listBarang;
        }

        public static string createData(M_tbl_Barang barang)
        {
            try
            {
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    barang.is_delete = false;
                    barang.Created_By = "System";
                    barang.Created_Date = DateTime.Now;

                    db.M_tbl_Barang.Add(barang);
                    db.SaveChanges();

                    return "ok";
                }
            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }

        }

        public static M_tbl_Barang getDataById(int id)
        {
            M_tbl_Barang data = new M_tbl_Barang();

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = db.M_tbl_Barang.Where(a => a.id_Barang == id).FirstOrDefault();
            }

            return data;
        }

        public static string updateData(M_tbl_Barang barang)
        {
            try
            {
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    M_tbl_Barang baranglama = new M_tbl_Barang();

                    baranglama = getDataById(barang.id_Barang);
                    baranglama.Kode_Barang = barang.Kode_Barang;
                    baranglama.Nama_Barang = barang.Nama_Barang;
                    baranglama.Harga = barang.Harga;
                    baranglama.Qty = barang.Qty;

                    baranglama.Upload_BY = "System";
                    baranglama.Upload_Date = DateTime.Now;

                    db.Entry(baranglama).State = EntityState.Modified;
                    db.SaveChanges();

                    return "ok";
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }

        public static string deleteData(int id)
        {
            try
            {
                M_tbl_Barang barang = new M_tbl_Barang();
                using (DBTokosidiaEntities db = new DBTokosidiaEntities())
                {
                    barang = getDataById(id);
                    barang.is_delete = true;
                    barang.Upload_BY = "System";
                    barang.Upload_Date = DateTime.Now;

                    db.Entry(barang).State = EntityState.Modified;
                    db.SaveChanges();

                    return "ok";
                }
            }
            catch (Exception e)
            {
                return e.Message.ToString();
            }
        }
    }
}
