﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Tokosidia;
using Model.Tokosidia;
using System.Data.Entity;
using Model.Tokosidia;
using ViewModel.Tokosidia;
using System.Data.Entity;

namespace Repo.Tokosidia
{
    public class CartRepo
    {
        public static List<VMTransaksi> getDataCartCustomer(int customer_id)
        {
            List<VMTransaksi> data = new List<VMTransaksi>();
            

            using (DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                data = (from b in db.M_tbl_Barang
                        join od in db.Order_Detail on b.id_Barang equals od.id_Barang
                        join oh in db.Order_Header on od.Kode_Transaksi equals oh.Kode_Transaksi
                        join cs in db.Customers on oh.Customer_Id equals cs.ID
                        where cs.ID == customer_id && oh.isCheckout == false

                        select new VMTransaksi
                        {
                            Kode_Transaksi = od.Kode_Transaksi,
                            Nama_Barang = b.Nama_Barang,
                            Harga = od.Harga,
                            Quantity = od.Quantity,
                        }
                        ).ToList();
            }
            
            return data;
        }

        public static decimal getTotalBelanja(int customer_id)
        {
            decimal total;

            using(DBTokosidiaEntities db = new DBTokosidiaEntities())
            {
                total = (from b in db.M_tbl_Barang
                        join od in db.Order_Detail on b.id_Barang equals od.id_Barang
                        join oh in db.Order_Header on od.Kode_Transaksi equals oh.Kode_Transaksi
                        join cs in db.Customers on oh.Customer_Id equals cs.ID
                        where cs.ID == customer_id && oh.isCheckout == false

                        select od.Harga
                        ).Sum();
            }

            return total;
        }
    }
}
