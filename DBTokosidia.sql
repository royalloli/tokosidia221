USE [DBTokosidia]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](30) NULL,
	[password] [varchar](30) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[NAMA] [varchar](100) NOT NULL,
	[NO_TLP] [varchar](15) NOT NULL,
	[EMAIL] [varchar](50) NOT NULL,
	[GENDER] [int] NULL,
	[INDENTITAS_TYPE] [int] NULL,
	[NO_INDENTITAS] [varchar](15) NOT NULL,
	[ALAMAT] [varchar](max) NULL,
	[KODE_PROVINSI] [varchar](7) NULL,
	[KODE_KOTA] [varchar](7) NULL,
	[KODE_KECAMATAN] [varchar](7) NULL,
	[KODE_KELURAHAN] [varchar](7) NULL,
	[ISDELETE] [bit] NULL,
	[CREATED_BY] [varchar](7) NULL,
	[CREATED_DATE] [datetime] NULL,
	[UPDATED_BY] [varchar](7) NULL,
	[UPDATED_DATE] [datetime] NULL,
	[ROLE_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kecamatan]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kecamatan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Kecamatan] [varchar](7) NOT NULL,
	[Kode_Kota] [varchar](7) NOT NULL,
	[Nama_Kecamatan] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kelurahan]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kelurahan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Kelurahan] [varchar](7) NOT NULL,
	[Kode_Kecamatan] [varchar](7) NOT NULL,
	[Nama_Kelurahan] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Kota]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kota](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Kota] [varchar](7) NOT NULL,
	[Kode_Provinsi] [varchar](7) NULL,
	[Nama_Kota] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[M_tbl_Barang]    Script Date: 11/22/2019 9:34:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[M_tbl_Barang](
	[id_Barang] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Barang] [varchar](7) NULL,
	[Nama_Barang] [varchar](50) NOT NULL,
	[Harga] [decimal](10, 2) NOT NULL,
	[Qty] [int] NOT NULL,
	[is_delete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Upload_BY] [varchar](7) NULL,
	[Upload_Date] [datetime] NULL,
	[img] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[id_Barang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Menu_Name] [varchar](80) NOT NULL,
	[Menu_Url] [varchar](80) NOT NULL,
	[Menu_Icon] [varchar](80) NULL,
	[Menu_Order] [int] NULL,
	[Menu_Parent] [int] NULL,
	[Menu_Type] [varchar](80) NULL,
	[is_delete] [bit] NULL,
	[Created_by] [varchar](7) NULL,
	[Created_Date] [date] NULL,
	[Updated_by] [varchar](7) NULL,
	[Updated_Date] [nchar](10) NOT NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu_Access]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu_Access](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Role_Id] [int] NULL,
	[Menu_id] [int] NULL,
	[is_delete] [bit] NULL,
	[Created_by] [varchar](7) NULL,
	[Created_Date] [date] NULL,
	[Updated_by] [varchar](7) NULL,
	[Updated_Date] [date] NULL,
 CONSTRAINT [PK_Menu_Access] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Detail]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Detail](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Transaksi] [varchar](7) NOT NULL,
	[id_Barang] [int] NULL,
	[Quantity] [int] NOT NULL,
	[Harga] [decimal](18, 2) NOT NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
 CONSTRAINT [PK_Order_Detail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Header]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Header](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Transaksi] [varchar](7) NOT NULL,
	[Customer_Id] [int] NOT NULL,
	[isCheckout] [bit] NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
 CONSTRAINT [PK_Order_Header] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provinsi]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provinsi](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Kode_Provinsi] [varchar](7) NOT NULL,
	[Nama_Provinsi] [varchar](50) NOT NULL,
	[isDelete] [bit] NULL,
	[Created_By] [varchar](7) NULL,
	[Created_Date] [datetime] NULL,
	[Updated_By] [varchar](7) NULL,
	[Updated_Date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/22/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Role_Name] [varchar](80) NULL,
	[is_delete] [bit] NULL,
	[Created_by] [varchar](7) NULL,
	[Created_Date] [date] NULL,
	[Updated_by] [varchar](7) NULL,
	[Updated_Date] [date] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([ID], [username], [password]) VALUES (1, N'admin1', N'admin1')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (2, N'Adi', N'08997110268', N'adi@gmail.com', 1, NULL, N'123456789', N'Bandung', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (3, N'Bina', N'089977776', N'email@gmail.com', NULL, NULL, N'1234567', N'Jakarta', NULL, NULL, NULL, NULL, 1, N'System', CAST(N'2019-11-14T10:54:56.630' AS DateTime), N'System', CAST(N'2019-11-14T13:14:32.880' AS DateTime), NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (4, N'Salman', N'098789377', N'salman@email.com', NULL, NULL, N'7654321', N'Surabaya', NULL, NULL, NULL, NULL, 1, N'System', CAST(N'2019-11-14T11:00:07.790' AS DateTime), N'System', CAST(N'2019-11-14T13:14:22.290' AS DateTime), NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (5, N'Bina', N'0899888678123', N'bina@gmail.com', 2, NULL, N'987654321', N'Kopo', NULL, NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-14T13:42:07.990' AS DateTime), N'System', CAST(N'2019-11-14T13:58:41.763' AS DateTime), NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (6, N'Salman', N'086854566', N'salman@email.com', 2, NULL, N'5233622', N'Bandung', NULL, NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-14T13:46:32.033' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (7, N'Andri', N'0868956', N'andri@gmail.com', 2, NULL, N'5233622', N'Soreang', NULL, NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-14T13:55:04.730' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (8, N'Rendi', N'086567', N'rendi@gmail.com', 2, NULL, N'764534', N'Jakarta', NULL, NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-14T15:18:20.063' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (9, N'Heru', N'98764567', N'email@gmail.com', 2, NULL, N'453423', N'Bandung', N'PR00003', NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-14T15:23:13.443' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (10, N'Rena', N'0876567', N'rena@gmail.com', 1, NULL, N'543423', N'Jl. Kota', N'PR00012', NULL, NULL, NULL, 0, N'System', CAST(N'2019-11-15T11:05:49.810' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (11, N'Reni', N'097567', N'reni@gmail.com', 1, NULL, N'4432342', N'jl.alamat', N'PR00011', N'KT00003', NULL, NULL, 0, N'System', CAST(N'2019-11-15T11:15:00.223' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (12, N'Reja', N'094567876', N'rezi@gmail.com', 2, NULL, N'764534', N'Jl. Jakarta', N'PR00011', N'KT00005', N'KC00009', N'KL00029', 1, N'System', CAST(N'2019-11-15T13:40:45.590' AS DateTime), N'System', CAST(N'2019-11-15T16:40:21.827' AS DateTime), NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (13, N'Bina', N'086567', N'bina@gmail.com', 2, NULL, N'5324', N'Jl. Kota No. 111', N'PR00011', N'KT00002', N'KC00004', N'KL00013', 0, N'System', CAST(N'2019-11-15T14:44:37.523' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[Customer] ([ID], [NAMA], [NO_TLP], [EMAIL], [GENDER], [INDENTITAS_TYPE], [NO_INDENTITAS], [ALAMAT], [KODE_PROVINSI], [KODE_KOTA], [KODE_KECAMATAN], [KODE_KELURAHAN], [ISDELETE], [CREATED_BY], [CREATED_DATE], [UPDATED_BY], [UPDATED_DATE], [ROLE_ID]) VALUES (14, N'Aku', N'09878333', N'aku@email.com', 2, NULL, N'5432454', N'Jl. Soreang No.123', N'PR00011', N'KT00001', N'KC00013', N'KL00039', 1, N'System', CAST(N'2019-11-15T15:49:22.387' AS DateTime), N'System', CAST(N'2019-11-15T15:49:31.940' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[Kecamatan] ON 

INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'KC00001', N'KT00003', N'Cilandak', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'KC00002', N'KT00003', N'Jagakarsa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'KC00003', N'KT00003', N'Kebayoran Baru', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'KC00004', N'KT00002', N'Cempaka Putih', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'KC00005', N'KT00002', N'Gambir', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'KC00006', N'KT00004', N'Kemayoran', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'KC00007', N'KT00005', N'Cilincing', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (8, N'KC00008', N'KT00005', N'Kelapa Gading', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (9, N'KC00009', N'KT00005', N'Koja', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (10, N'KC00010', N'KT00004', N'Cakung', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (11, N'KC00011', N'KT00004', N'Cipayung', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (12, N'KC00012', N'KT00004', N'Ciracas', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (13, N'KC00013', N'KT00001', N'Cengkareng', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (14, N'KC00014', N'KT00001', N'Grogol Petamburan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
INSERT [dbo].[Kecamatan] ([id], [Kode_Kecamatan], [Kode_Kota], [Nama_Kecamatan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (15, N'KC00015', N'KT00001', N'Taman Sari', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.047' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Kecamatan] OFF
SET IDENTITY_INSERT [dbo].[Kelurahan] ON 

INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'KL00001', N'KC00001', N'Cilandak Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'KL00002', N'KC00001', N'Cilandak Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'KL00003', N'KC00001', N'Gandaria Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'KL00004', N'KC00001', N'Lebak Bulus', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'KL00005', N'KC00001', N'Pondok Labu', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'KL00006', N'KC00002', N'Ciganjur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'KL00007', N'KC00002', N'Cipedak', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (8, N'KL00008', N'KC00002', N'Jagakarsa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (9, N'KL00009', N'KC00003', N'Jagakarsa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (10, N'KL00010', N'KC00003', N'Jagakarsa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (11, N'KL00011', N'KC00003', N'Jagakarsa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (12, N'KL00012', N'KC00004', N'Cempaka Putih Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (13, N'KL00013', N'KC00004', N'Cempaka Putih Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (14, N'KL00014', N'KC00004', N'Rawasari', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (15, N'KL00015', N'KC00005', N'Cideng', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (16, N'KL00016', N'KC00005', N'Duri Pulo', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (17, N'KL00017', N'KC00005', N'Gambir', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (18, N'KL00018', N'KC00006', N'Galur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (19, N'KL00019', N'KC00006', N'Johar Baru', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (20, N'KL00020', N'KC00006', N'Kampung Rawa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (21, N'KL00021', N'KC00007', N'Cilincing', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (22, N'KL00022', N'KC00007', N'Kalibaru', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (23, N'KL00023', N'KC00007', N'Marunda', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (24, N'KL00024', N'KC00008', N'Kelapa Gading Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (25, N'KL00025', N'KC00008', N'Kelapa Gading Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (26, N'KL00026', N'KC00008', N'Pegangsaan Dua', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (27, N'KL00027', N'KC00009', N'Koja', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (28, N'KL00028', N'KC00009', N'Kojaoa', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (29, N'KL00029', N'KC00009', N'Rawa Badak Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (30, N'KL00030', N'KC00010', N'Cakung Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (31, N'KL00031', N'KC00010', N'Cakung Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (32, N'KL00032', N'KC00010', N'Jatinegara', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (33, N'KL00033', N'KC00011', N'Bambu Apus', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (34, N'KL00034', N'KC00011', N'Ceger', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (35, N'KL00035', N'KC00011', N'Cilangkap', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (36, N'KL00036', N'KC00012', N'Cibubur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (37, N'KL00037', N'KC00012', N'Ciracas', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (38, N'KL00038', N'KC00012', N'Rambutan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (39, N'KL00039', N'KC00013', N'Kapuk', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (40, N'KL00040', N'KC00013', N'Kedaung Kali Angke', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (41, N'KL00041', N'KC00013', N'Rawa Buaya', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (42, N'KL00042', N'KC00014', N'Grogol', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (43, N'KL00043', N'KC00014', N'Jelambar Baru', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (44, N'KL00044', N'KC00014', N'Jelambar', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (45, N'KL00045', N'KC00014', N'Grogol', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (46, N'KL00046', N'KC0001', N'Jelambar Baru', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (47, N'KL00047', N'KC00014', N'Jelambar', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (48, N'KL00048', N'KC00015', N'Glodok', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (49, N'KL00049', N'KC00015', N'Keagungan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
INSERT [dbo].[Kelurahan] ([id], [Kode_Kelurahan], [Kode_Kecamatan], [Nama_Kelurahan], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (50, N'KL00050', N'KC00015', N'Krukut', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.113' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Kelurahan] OFF
SET IDENTITY_INSERT [dbo].[Kota] ON 

INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'KT00001', N'PR00011', N'Administrasi Jakarta Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'KT00002', N'PR00011', N'Administrasi Jakarta Pusat', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'KT00003', N'PR00011', N'Administrasi Jakarta Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'KT00004', N'PR00011', N'Administrasi Jakarta Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'KT00005', N'PR00011', N'Administrasi Jakarta Utara', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'KT00006', N'PR00012', N'Bandung', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'KT00007', N'PR00012', N'Bekasi', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (8, N'KT00008', N'PR00012', N'Bogor', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (9, N'KT00009', N'PR00012', N'Cimahi', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (10, N'KT00010', N'PR00012', N'Cirebon', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (11, N'KT00011', N'PR00012', N'Depok', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (12, N'KT00012', N'PR00012', N'Sukabumi', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (13, N'KT00013', N'PR00012', N'Tasikmalaya', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
INSERT [dbo].[Kota] ([id], [Kode_Kota], [Kode_Provinsi], [Nama_Kota], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (14, N'KT00014', N'PR00012', N'Banjar', 0, N'PG00001', CAST(N'2019-11-13T16:34:29.000' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Kota] OFF
SET IDENTITY_INSERT [dbo].[M_tbl_Barang] ON 

INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (1, N'BR00001', N'Indomie Goreng', CAST(2500.00 AS Decimal(10, 2)), 1, 1, N'PG00001', CAST(N'2019-11-08T14:56:15.483' AS DateTime), N'System', CAST(N'2019-11-13T09:31:53.680' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (2, N'K000002', N'Jeruk', CAST(10000.00 AS Decimal(10, 2)), 9, 1, N'System', CAST(N'2019-11-08T17:28:31.153' AS DateTime), N'System', CAST(N'2019-11-12T15:11:50.410' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (3, N'K000003', N'Apel', CAST(9000.00 AS Decimal(10, 2)), 12, 1, N'System', CAST(N'2019-11-08T17:29:22.427' AS DateTime), N'System', CAST(N'2019-11-12T15:10:53.467' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (4, N'K000004', N'Buku', CAST(30000.00 AS Decimal(10, 2)), 9, 1, N'System', CAST(N'2019-11-12T11:05:53.550' AS DateTime), N'System', CAST(N'2019-11-12T15:10:53.467' AS DateTime), N'5.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (5, N'K000005', N'Pensil', CAST(10000.00 AS Decimal(10, 2)), 5, 1, N'System', CAST(N'2019-11-12T13:48:14.950' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (6, N'K000006', N'Penghapus', CAST(10000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-12T13:57:58.627' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (7, N'K000007', N'Penggaris', CAST(20000.00 AS Decimal(10, 2)), 3, 1, N'System', CAST(N'2019-11-12T13:58:11.303' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (8, N'K000001', N'Meja', CAST(301000.00 AS Decimal(10, 2)), 9, 1, N'System', CAST(N'2019-11-12T15:15:06.613' AS DateTime), N'System', CAST(N'2019-11-12T15:15:12.080' AS DateTime), N'1.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (9, N'KB00002', N'Pensil', CAST(10000.00 AS Decimal(10, 2)), 12, 1, N'System', CAST(N'2019-11-12T15:54:57.840' AS DateTime), N'System', CAST(N'2019-11-12T15:56:52.930' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (10, N'KC00003', N'Laptop', CAST(4500000.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T09:23:54.860' AS DateTime), N'System', CAST(N'2019-11-13T09:24:11.847' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (11, N'KT00006', N'Batu', CAST(100.00 AS Decimal(10, 2)), 4, 1, N'System', CAST(N'2019-11-13T09:30:56.617' AS DateTime), N'System', CAST(N'2019-11-13T09:31:13.223' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (12, N'KB00004', N'Hp', CAST(100000.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T09:31:34.500' AS DateTime), N'System', CAST(N'2019-11-13T09:31:53.680' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (13, N'K00002', N'PC', CAST(100000.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T09:32:42.950' AS DateTime), N'System', CAST(N'2019-11-13T09:37:43.220' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (14, N'KV00002', N'Monitor', CAST(900000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T09:36:07.850' AS DateTime), N'System', CAST(N'2019-11-13T11:10:40.813' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (15, N'KL00008', N'IC', CAST(890000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T09:36:28.627' AS DateTime), N'System', CAST(N'2019-11-13T11:38:52.973' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (16, N'KJ00002', N'Batu', CAST(80000.00 AS Decimal(10, 2)), 20, 1, N'System', CAST(N'2019-11-13T10:30:30.433' AS DateTime), N'System', CAST(N'2019-11-13T11:39:49.527' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (17, N'kd002', N'mie sedaap goreng', CAST(2500.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T11:39:23.173' AS DateTime), N'System', CAST(N'2019-11-13T11:42:06.133' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (18, N'kd004', N'Batu', CAST(100000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T11:39:36.830' AS DateTime), N'System', CAST(N'2019-11-13T11:42:23.387' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (19, N'kd002', N'mie sedaap goreng', CAST(2500.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T11:42:19.180' AS DateTime), N'System', CAST(N'2019-11-13T11:43:02.487' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (20, N'kd003', N'Kerupuk Ikan', CAST(100000.00 AS Decimal(10, 2)), 20, 1, N'System', CAST(N'2019-11-13T11:42:44.487' AS DateTime), N'System', CAST(N'2019-11-13T15:23:40.273' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (21, N'kd004', N'jamu', CAST(10000.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T11:42:57.180' AS DateTime), N'System', CAST(N'2019-11-13T13:33:30.410' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (22, N'kd005', N'susu', CAST(80000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T13:30:56.840' AS DateTime), N'System', CAST(N'2019-11-13T13:33:16.853' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (23, N'kd004', N'jamu', CAST(2500.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T13:33:46.340' AS DateTime), N'System', CAST(N'2019-11-13T13:34:12.370' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (24, N'kd002', N'Biskuit', CAST(10000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T13:34:00.993' AS DateTime), N'System', CAST(N'2019-11-13T15:06:49.340' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (25, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:32.567' AS DateTime), N'System', CAST(N'2019-11-13T14:22:42.580' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (26, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:33.367' AS DateTime), N'System', CAST(N'2019-11-13T14:22:45.147' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (27, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:33.530' AS DateTime), N'System', CAST(N'2019-11-13T14:22:48.210' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (28, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:33.697' AS DateTime), N'System', CAST(N'2019-11-13T14:22:50.973' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (29, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:33.883' AS DateTime), N'System', CAST(N'2019-11-13T14:22:53.650' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (30, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:34.080' AS DateTime), N'System', CAST(N'2019-11-13T14:22:59.353' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (31, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:34.273' AS DateTime), N'System', CAST(N'2019-11-13T14:22:56.673' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (32, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:34.457' AS DateTime), N'System', CAST(N'2019-11-13T14:23:04.507' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (33, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:34.690' AS DateTime), N'System', CAST(N'2019-11-13T14:23:36.793' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (34, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:37.657' AS DateTime), N'System', CAST(N'2019-11-13T14:23:40.480' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (35, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:37.830' AS DateTime), N'System', CAST(N'2019-11-13T14:23:43.393' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (36, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.010' AS DateTime), N'System', CAST(N'2019-11-13T14:23:46.680' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (37, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.197' AS DateTime), N'System', CAST(N'2019-11-13T14:23:34.090' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (38, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.393' AS DateTime), N'System', CAST(N'2019-11-13T14:23:50.433' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (39, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.590' AS DateTime), N'System', CAST(N'2019-11-13T14:23:31.097' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (40, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.743' AS DateTime), N'System', CAST(N'2019-11-13T14:23:53.583' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (41, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:38.930' AS DateTime), N'System', CAST(N'2019-11-13T14:23:26.977' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (42, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:39.103' AS DateTime), N'System', CAST(N'2019-11-13T14:23:20.633' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (43, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:39.327' AS DateTime), N'System', CAST(N'2019-11-13T14:23:23.977' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (44, NULL, N'buku', CAST(10.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:21:39.520' AS DateTime), N'System', CAST(N'2019-11-13T14:23:17.117' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (45, NULL, N'Batu', CAST(2500.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:22:25.807' AS DateTime), N'System', CAST(N'2019-11-13T14:23:13.410' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (46, NULL, N'Batu', CAST(2500.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:22:25.807' AS DateTime), N'System', CAST(N'2019-11-13T14:23:09.307' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (47, N'kd002', N'Buku', CAST(10000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T14:24:09.747' AS DateTime), N'System', CAST(N'2019-11-13T15:06:44.037' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (48, N'KO00001', N'Pensil', CAST(1000000.00 AS Decimal(10, 2)), 8, 1, N'System', CAST(N'2019-11-13T14:31:27.867' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (49, N'KO00001', N'Pensil', CAST(1000000.00 AS Decimal(10, 2)), 8, 1, N'System', CAST(N'2019-11-13T14:31:35.517' AS DateTime), N'System', CAST(N'2019-11-13T14:35:09.733' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (50, N'KO00001', N'Pensil', CAST(1000000.00 AS Decimal(10, 2)), 8, 1, N'System', CAST(N'2019-11-13T14:31:36.360' AS DateTime), N'System', CAST(N'2019-11-13T14:31:48.227' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (51, N'KJ00002', N'NoteBook', CAST(9100000.00 AS Decimal(10, 2)), 1, 1, N'System', CAST(N'2019-11-13T14:35:39.957' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (52, N'KJ00002', N'NoteBook', CAST(9100000.00 AS Decimal(10, 2)), 1, 1, N'System', CAST(N'2019-11-13T14:35:40.520' AS DateTime), N'System', CAST(N'2019-11-13T14:36:17.067' AS DateTime), NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (53, N'kd00004', N'Batu', CAST(100000.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T14:41:25.450' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (54, N'KJ00004', N'jamu', CAST(2500.00 AS Decimal(10, 2)), 7, 1, N'System', CAST(N'2019-11-13T14:50:21.173' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (55, N'kd00005', N'Batu Bata', CAST(900000.00 AS Decimal(10, 2)), 2, 1, N'System', CAST(N'2019-11-13T14:54:17.240' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (56, N'KB00002', N'Baju Heru', CAST(90000.00 AS Decimal(10, 2)), 1, 1, N'System', CAST(N'2019-11-13T14:55:12.747' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (57, N'KJ00012', N'Baju', CAST(890000.00 AS Decimal(10, 2)), 9, 0, N'System', CAST(N'2019-11-13T15:02:50.253' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (58, N'KJ00023', N'Bato', CAST(770000.00 AS Decimal(10, 2)), 7, 0, N'System', CAST(N'2019-11-13T16:23:31.210' AS DateTime), N'System', CAST(N'2019-11-13T16:25:16.837' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (59, N'KJ00013', N'NoteBook', CAST(900000.00 AS Decimal(10, 2)), 10, 0, N'System', CAST(N'2019-11-13T16:26:42.593' AS DateTime), N'System', CAST(N'2019-11-13T16:38:23.603' AS DateTime), N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (60, N'KJ00012', N'Batu', CAST(100000.00 AS Decimal(10, 2)), 9, 0, N'System', CAST(N'2019-11-13T16:47:34.470' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (61, N'KJ00015', N'Biskuit', CAST(10000.00 AS Decimal(10, 2)), 4, 0, N'System', CAST(N'2019-11-13T16:48:48.057' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (62, N'KJ00016', N'Charger', CAST(9000.00 AS Decimal(10, 2)), 1, 0, N'System', CAST(N'2019-11-13T16:55:09.680' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (63, N'KU00013', N'Topi', CAST(90000.00 AS Decimal(10, 2)), 1, 0, N'System', CAST(N'2019-11-13T16:58:10.303' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (64, N'KJ00022', N'Heri', CAST(80000.00 AS Decimal(10, 2)), 1, 0, N'System', CAST(N'2019-11-13T16:59:01.777' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (65, N'AQ00001', N'Aqua', CAST(10000.00 AS Decimal(10, 2)), 3, 0, N'System', CAST(N'2019-11-20T17:05:07.550' AS DateTime), NULL, NULL, N'1.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (66, N'AQ00001', N'Warung', CAST(10000.00 AS Decimal(10, 2)), 3, 0, N'System', CAST(N'2019-11-20T17:23:48.047' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (67, N'AQ00010', N'Sanqua', CAST(20000.00 AS Decimal(10, 2)), 1, 0, N'System', CAST(N'2019-11-20T17:31:20.573' AS DateTime), NULL, NULL, N'default.jpg')
INSERT [dbo].[M_tbl_Barang] ([id_Barang], [Kode_Barang], [Nama_Barang], [Harga], [Qty], [is_delete], [Created_By], [Created_Date], [Upload_BY], [Upload_Date], [img]) VALUES (68, N'AS00001', N'Jam', CAST(9000.00 AS Decimal(10, 2)), 8, 0, N'System', CAST(N'2019-11-20T17:34:54.163' AS DateTime), NULL, NULL, N'drivethru.jpg')
SET IDENTITY_INSERT [dbo].[M_tbl_Barang] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([Id], [Menu_Name], [Menu_Url], [Menu_Icon], [Menu_Order], [Menu_Parent], [Menu_Type], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (4, N'Home', N'../Home/Index', N'w', 1, NULL, N'sidebar', 0, NULL, NULL, NULL, N'11/11/19  ')
INSERT [dbo].[Menu] ([Id], [Menu_Name], [Menu_Url], [Menu_Icon], [Menu_Order], [Menu_Parent], [Menu_Type], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (6, N'Barang', N'../Barang/Index', NULL, 2, NULL, N'sidebar', NULL, NULL, NULL, NULL, N'11/11/19  ')
INSERT [dbo].[Menu] ([Id], [Menu_Name], [Menu_Url], [Menu_Icon], [Menu_Order], [Menu_Parent], [Menu_Type], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (7, N'Customer', N'../Customer/Index', NULL, 3, NULL, N'sidebar', NULL, NULL, NULL, NULL, N'11/11/19  ')
INSERT [dbo].[Menu] ([Id], [Menu_Name], [Menu_Url], [Menu_Icon], [Menu_Order], [Menu_Parent], [Menu_Type], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (8, N'Cart', N'../Cart/Index', NULL, 4, NULL, N'sidebar', NULL, NULL, NULL, NULL, N'11/11/19  ')
SET IDENTITY_INSERT [dbo].[Menu] OFF
SET IDENTITY_INSERT [dbo].[Menu_Access] ON 

INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (1, 1, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (2, 1, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (3, 1, 7, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (4, 1, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (5, 3, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (6, 3, 6, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (7, 3, 8, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (8, 2, 4, 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Menu_Access] ([Id], [Role_Id], [Menu_id], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (9, 2, 8, 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Menu_Access] OFF
SET IDENTITY_INSERT [dbo].[Order_Detail] ON 

INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'TX0001', NULL, 3, CAST(2670000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'TX0001', NULL, 3, CAST(30000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'TX0001', NULL, 2, CAST(20000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'TX0001', NULL, 5, CAST(50000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'TX0001', NULL, 2, CAST(20000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'TX0001', NULL, 1, CAST(9000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Detail] ([Id], [Kode_Transaksi], [id_Barang], [Quantity], [Harga], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'TX0001', NULL, 3, CAST(2310000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Order_Detail] OFF
SET IDENTITY_INSERT [dbo].[Order_Header] ON 

INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'TX0001', 0, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Order_Header] ([Id], [Kode_Transaksi], [Customer_Id], [isCheckout], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'TX0001', 2, 0, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Order_Header] OFF
SET IDENTITY_INSERT [dbo].[Provinsi] ON 

INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (1, N'PR00001', N'Aceh', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (2, N'PR00002', N'Sumatra Utara', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (3, N'PR00003', N'Sumatra Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (4, N'PR00004', N'Riau', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (5, N'PR00005', N'Jambi', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (6, N'PR00006', N'Sumatra Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (7, N'PR00007', N'Bengkulu', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (8, N'PR00008', N'Lampung', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (9, N'PR00009', N'Bangka Belitung', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (10, N'PR00010', N'Kepulauan Riau', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (11, N'PR00011', N'Jakarta', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (12, N'PR00012', N'Jawa Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (13, N'PR00013', N'Jawa Tengah', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (14, N'PR00014', N'Yogyakarta', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (15, N'PR00015', N'Jawa Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (16, N'PR00016', N'Banten', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (17, N'PR00017', N'Bali', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (18, N'PR00018', N'Nusa Tenggara Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (19, N'PR00019', N'Nusa Tenggara Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (20, N'PR00020', N'Kalimantan Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (21, N'PR00021', N'Kalimantan Tengah', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (22, N'PR00022', N'Kalimantan Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (23, N'PR00023', N'Kalimantan Timur', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (24, N'PR00024', N'Kalimantan Utara', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (25, N'PR00025', N'Sulawesi Utara', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (26, N'PR00026', N'Sulawesi Tengah', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (27, N'PR00027', N'Sulawesi Selatan', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (28, N'PR00028', N'Sulawesi Tenggara', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (29, N'PR00029', N'Gorontalo', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (30, N'PR00030', N'Sulawesi Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (31, N'PR00031', N'Maluku', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (32, N'PR00032', N'Maluku Utara', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (33, N'PR00033', N'Papua', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
INSERT [dbo].[Provinsi] ([id], [Kode_Provinsi], [Nama_Provinsi], [isDelete], [Created_By], [Created_Date], [Updated_By], [Updated_Date]) VALUES (34, N'PR00034', N'Papua Barat', 0, N'PG00001', CAST(N'2019-11-13T16:34:28.890' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Provinsi] OFF
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([Id], [Role_Name], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (1, N'Admin', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Role_Name], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (2, N'Pembeli', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[Role] ([Id], [Role_Name], [is_delete], [Created_by], [Created_Date], [Updated_by], [Updated_Date]) VALUES (3, N'Penjual', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Role] OFF
